#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <iomanip>
#include <unistd.h>
using namespace std;
struct order
{
    int prodid1;
    char pname1[50];
    int qty1;
    float price1, dis1,etc1;
} o1[50];
string create_file(string);
int Login();

void write_customer();
void administratormenu();
void copyme(int k, order order1[], int q1, int &c2);
int getproduct();
int getcustomers();
void product_detail_heading();
void customer_detail_heading();
void prod_tabular();
void cust_menu1();
void cust_menu2();
void cust_tabular();
void modify_record(int n);
void delete_record(int n);
void againopenandclose();
void againopenandclosecust();
int search(int p);
void changeqty(int pr1, int q11);
void write_book();
void identify();

class customer
{
    int cust_id;
    char cname[100];
    char address[100];
    char phno[15];

public:
    
    int getcustid()
    {
        return cust_id;
    }
    char *getcustnm()
    {
        return cname;
    }
    char *getcustadd()
    {
        return address;
    }
    char *getphno()
    {
        return phno;
    }
    // function for taking input from customer
    void cust_input(int custid)
    {
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "CUSTOMER NO: ";
        cust_id = custid;
        cout << cust_id << endl;
        cout << "NAME OF CUSTOMER:" << endl;
        cin >> cname;
        cout << "ADDRESS:" << endl;
        cin >> address;
        cout << "PHONE NO.:" << endl;
        cin >> phno;
        cout << "-------------------------------------------------------------------------" << endl;
    }
    // function to show customer details
    void showallcust(int c)
    {
        cout << "   " << cust_id << setw(15) << cname << setw(23) << address << setw(27) << phno << endl;
    }
    void show_cust()
    {
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "CUSTOMER NO      : " << cust_id << endl;
        cout << "NAME OF CUSTOMER : " << cname << endl;
        cout << "ADDRESS    : " << address << endl;
        cout << "PHONE NO.  : " << phno << endl;
        cout << "-------------------------------------------------------------------------" << endl;
    }
};
void display_cust_sp(int n)
{
       ifstream objfp;
       customer cust;
       int flag=0;
       objfp.open("customer.csv",ios::binary);
       if(!objfp)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
            return;
      }
      while(objfp.read((char*)&cust,sizeof(customer)))
      {
             if(cust.getcustid()==n)
            {
                  cust.show_cust();
                  flag=1;
            }
      }
      objfp.close();
      if(flag==0)
      cout<<"\n\nRecord doesnot exist"<<endl;
      cin.get();
}

    void customer_menu()
{
      char ch2;
      int num;
      cout<<"\nCUSTOMERS MENU"<<endl;
      cout<<"1.DISPLAY ALL CUSTOMERS DETAILS"<<endl;
      cout<<"2.SEARCH RECORD (QUERY) "<<endl;
      cout<<"3.GO BACK "<<endl;
      cout<<"Please Enter Your Choice (1-3) "<<endl;
      cin>>ch2;
      switch(ch2)
      {
            case '1':
                  cust_tabular();
                  customer_menu();
                  break;
            case '2':
                  cout<<"ENTER THE CUST ID TO BE SEARCHED:"<<endl;
                  cin>>num;
                  display_cust_sp(num);
                  customer_menu();
                  break;
            case '3':
                  //administratormenu();
                  break;
            default:
                  cout<<"Please enter valid option"<<endl;
      }
}

class product
{
    int food_id;
    char name[50];
    int qty;
    float price, dis, etc;

public:
    product()
    {
        qty = 0;
        price = 0;
        dis = 0;
        etc=0;
    }
    void create_prod(int rn1)
    {
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "Food No: ";
        food_id = rn1;
        cout << food_id << endl;
        cout << "Food Item:" << endl;
        cin >> name;
        cout << "Price:" << endl;
        cin >> price;
        cout << "Quantity:" << endl;
        cin >> qty;
        cout << "Discount%:" << endl;
        cin >> dis;
        cout << "Food Preparation Time:" << endl;
        cin >> etc;
        cout << "-------------------------------------------------------------------------" << endl;
    }
    void show_prod()
    {
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "Food Item No.          : " << food_id << endl;
        cout << "Food Name              : " << name << endl;
        cout << "Price                  : " << price << endl;
        cout << "Quantity               : " << qty << endl;
        cout << "Discount%              : " << dis << "%" << endl;
        cout << "Food Preparation Time  : " << etc << endl;
        cout << "-------------------------------------------------------------------------" << endl;
    }
    // Function shows product data in tabular form
    void showall(int c)
    {
        cout << "  " << food_id << setw(15) << name << setw(11) << "Rs." << price << setw(12) << qty << setw(13) << dis << "%" << setw(12) << etc << endl;
    }
    int retpno()
    {
        return food_id;
    }
    float retprice()
    {
        return price;
    }
    float foodPrep()
    {
        return etc;
    }
    char *getname()
    {
        return name;
    }
    int getqty()
    {
        return qty;
    }
    float retdis()
    {
        return dis;
    }
    void setqty(int q21)
    {
        qty = q21;
    }
}pr;
// Global declaration for stream object
fstream fp;
void display_sp(int n)
{
      int flag=0;
      fp.open("product.csv",ios::in);
      if(!fp)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
            return;
      }
      while(fp.read((char*)&pr,sizeof(product)))
      {
            if(pr.retpno()==n)
            {
                  pr.show_prod();
                  flag=1;
            }
      }
      fp.close();
      if(flag==0)
            cout<<"\n\nRecord doesnot exist"<<endl;
      cin.get();
}
void product_menu()
{
      char ch2;
      int num;
      cout<<"\n==========================   PRODUCTS MENU   ==========================="<<endl;
      cout<<"1.CREATE PRODUCTS"<<endl;
      cout<<"2.DISPLAY ALL PRODUCTS AVAILABLE"<<endl;
      cout<<"3.SEARCH RECORD (QUERY) "<<endl;
      cout<<"4.BACK TO MAIN MENU"<<endl;
      cout<<"Please Enter Your Choice (1-4) "<<endl;
      cin>>ch2;
      switch(ch2)
      {
            case '1':
                  write_book();
                  product_menu();
                  break;
            case '2':
                  prod_tabular();
                  break;
            case '3':
                  cout<<"\nENTER THE PRODUCT ID TO BE SEARCHED:"<<endl;
                  cin>>num;
                  display_sp(num);
                  product_menu();
                  break;
            case '4':
                  administratormenu();
                  break;
            default:
                  cout<<"a";
                  product_menu();
      }
}
// Function to write product details in file
int getproduct()
{
      ifstream objiff;
      product st;
      int count=0;
      objiff.open("product.csv",ios::binary);
      objiff.seekg(0,ios::beg);
      if(!objiff)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
      }
      while(objiff.read((char *) &st, sizeof(product)))
      {
            count++;
      }
      objiff.seekg(count-sizeof(st),ios::beg);
      objiff.read((char *) &st, sizeof(product));
      count=st.retpno();
      count++;
      objiff.close();
      return count;
}
void write_book()
{
      fp.open("product.csv",ios::out|ios::app);
      int rnn=getproduct();
      if(rnn>100)
      {
            rnn=1;
      }
      pr.create_prod(rnn);
      fp.write((char*)&pr,sizeof(product));
      fp.close();
      cout<<"***********************  PRODUCTS RECORD SAVED  ************************"<<endl;
      cin.ignore();
      cin.get();
}
void write_customer()
{
      ofstream objoff;
      customer cobj;
      objoff.open("customer.csv",ios::out|ios::app);
      int r=getcustomers();
    //   if(r>100) //1000
    //   {
    //         r=1; // r=100
    //   }
       cobj.cust_input(r);
       objoff.write((char*)&cobj,sizeof(customer));
       objoff.close();
       cout<<"***********************   CUSTOMER RECORD SAVED   ***********************"<<endl;
       cin.ignore();
       cin.get();
}
//Function to check the customer number already given or not
int getcustomers()
{
      ifstream objiff;
      customer cust;
      int count=0;
      objiff.open("customer.csv",ios::binary);
      objiff.seekg(0,ios::beg);
      if(!objiff)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
      }
      while(objiff.read((char *) &cust, sizeof(customer)))
      {
            count++;
      }
      //***********jump to the last line
      objiff.seekg(count-sizeof(cust),ios::beg);
      objiff.read((char *) &cust, sizeof(customer));
      count=cust.getcustid();
      count++;
      objiff.close();
      return count;
}
void cust_tabular()
{
      int r=0,col=10;
      customer cust;
      ifstream inFile;
      inFile.open("customer.csv",ios::binary);
      if(!inFile)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
            return;
      }
      customer_detail_heading();
      while(inFile.read((char *) &cust, sizeof(customer)))
      {
             if(r<=12)
             {
                   r++;
                   cust.showallcust(col);
                   col++;
             }
             else
             {
                   cout<<"----------------------------- Press any key -----------------------------"<<endl;
                   cin.get();
                   customer_detail_heading();
                   col=10;
                   r=0;
            }
      }
      inFile.close();
      cin.get();
}
// Function to add the records in file

void displayCommon()
{
    cout << endl;
    cout << "--------------------------------------------------------------------------------------------" << endl;
    cout << "*************************    I N T I M E T E C  C A N T E E N     **************************" << endl;
    cout << "--------------------------------------------------------------------------------------------" << endl;
    cout << endl;
}
int search(int p)
{
    product st;
    int tmprt = 0;
    ifstream inFile;
    inFile.open("product.csv", ios::binary);
    if (!inFile)
    {
        cout << "File could not be open !! Press any Key..." << endl;
        cin.get();
        return -1;
    }
    int flag = 0;
    while (inFile.read((char *)&st, sizeof(product)))
    {
        if (st.retpno() == p)
        {
            st.show_prod();
            flag = 1;
            tmprt = (int)inFile.tellg();
            break;
        }
    }
    inFile.close();
    if (flag == 0)
        return 1;
    else
    {
        return tmprt;
    }
}
int before_order()
{
      int f=-1,num=0;
      customer cust;
      cout<<"ENTER THE CUSTOMER ID TO BILL:"<<endl;
      cin>>num;
      ifstream inFile;
      inFile.open("customer.csv",ios::binary);
      if(!inFile)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
            return -1;
      }
      while(inFile.read((char *) &cust, sizeof(customer)))
      {
            if(cust.getcustid()==num)
            {
                   cust.show_cust();
                   f=1;
                   //tmprt=(int)inFile.tellg();
                   break;
            }
      }
      inFile.close();
      return f;
}
void product_detail_heading()
{
      cout<<"                            PRODUCTS DETAILS                            "<<endl;
      cout<<"------------------------------------------------------------------------" << endl;
      cout<<"ITEM.NO"<<setw(12)<<"NAME"<<setw(13)<<"PRICE"<<setw(13)<<"QUANTITY"<<setw(12)<<"DISCOUNT"<<setw(15)<<"EXPECTED TIME"<<endl;
      cout<<"------------------------------------------------------------------------"<<endl;
}
void prod_tabular()
{
      int r=0,col=10;
      product st;
      ifstream inFile;
      inFile.open("product.csv",ios::binary);
      if(!inFile)
      {
            cout<<"File could not be open !! Press any Key..."<<endl;
            cin.get();
            return;
      }
      product_detail_heading();
      while(inFile.read((char *) &st, sizeof(product)))
      {
            if(r<=12)
            {
                  r++;
                  st.showall(col);
                  col++;
            }
            else
            {
                  cout<<"----------------------------- Press any key ----------------------------"<<endl;
                  cin.get();
                  product_detail_heading();
                  col=10;
                  r=0;
            }
      }
      inFile.close();
      cin.get();
}
void copyme(int k2,order order1[50],int q1,int &c2)
{
      ifstream objiff2("product.csv",ios::binary);
      product bk1;
      objiff2.seekg(k2-sizeof(product));
      objiff2.read((char*)&bk1,sizeof(product));
      strcpy(order1[c2].pname1,bk1.getname());
      order1[c2].dis1=bk1.retdis();
      order1[c2].price1=bk1.retprice();
      order1[c2].etc1=bk1.foodPrep();
      //COPY RECORD
      order1[c2].qty1=q1;
      c2++;
      objiff2.close();
}

void place_order(string cust_name)
{
    order o1[50];
    int c = 0, pr1 = 0;
    float amt = 0, damt = 0, total = 0,maxeta;
    int k = 0, q1;
    char ch = 'Y';
    int ptx[100];
    int v = 0;
    int value = before_order();
    if (value == 1)
    {
        cout << endl;

        do
        {
            prod_tabular();
            cout << "                             PLACE YOUR ORDER                           " << endl;
            cout << "ENTER THE PRODUCT NO: " << endl;
            cin >> pr1;
            k = search(pr1);
            if (k > 0)
            {
                cout << "Enter the Quantity:" << endl;
                cin >> q1;
                copyme(k, o1, q1, c);

                v++;
            }
            else
            {
                cout << "PRODUCT not found" << endl;
            }
            cout << "Do you want purchase more ? (Yes[ y or Y ] or NO [n or N])" << endl;
            cin >> ch;
        } while (ch == 'y' || ch == 'Y');
        cout << "Thank You For Placing The Order  ........" << endl
             << endl;
        cin.get();
             cout<< endl;
        cout << "*****************************   INVOICE   ******************************" << endl;
        cout << "Food.No." << setw(7) << "NAME" << setw(10) << "Qty" << setw(12) << "Price" << setw(13) << "Amount" << setw(23) << "Amount - discount" << endl
             << endl;
        
        //call here
        string cus_file=create_file(cust_name);
        ofstream cus_det;
        cus_det.open(cus_file,ios::app);
        if(!cus_det){
            cout<<"couldn't open file!\n";
            cin.get();
            return;
        }
        else 
            {
                
                for (int x = 0; x < c; x++)
                {
            amt = o1[x].qty1 * o1[x].price1;
            damt = amt - o1[x].dis1;
            cout << "  " << ptx[x] << setw(10) << o1[x].pname1 << setw(9) << o1[x].qty1 << setw(12) << "Rs." << o1[x].price1 << setw(10) << "Rs." << amt << setw(14) << "Rs." << damt << endl;
            cus_det << "  " <<  o1[x].pname1 << setw(9) << o1[x].qty1 << setw(12) << "Rs." << o1[x].price1 << setw(10) << "Rs." << amt << setw(14) << "Rs." << damt << endl;
            total += damt;
            maxeta=max(maxeta,o1[x].etc1);
             }
            }
            
        ofstream admin_ord_det;
        admin_ord_det.open("AdminOrderHistory.txt",ios::app);
        if(!admin_ord_det){
            cout<<"couldn't open file!\n";
            cin.get();
            return;
        }
        else 
            {
                
                for (int x = 0; x < c; x++)
                {
            admin_ord_det << "  " << o1[x].pname1 << setw(9) << o1[x].qty1 << setw(12) << "Rs." << o1[x].price1 << setw(10) << "Rs." << amt << setw(14) << "Rs." << damt << endl;
                
             }
            }

        cout << "\n-------------------------------------------------------------------------" << endl;
        
        cout << "\n		  TOTAL AMOUNT     :   "
             << "Rs." << total << endl;
        
        cout << "-------------------------------------------------------------------------" << endl;
        cout << "		   P A Y M E N T  C O M P L E T E D  " << endl;
        cout << "-------------------------------------------------------------------------" << endl;

        cout << "\n\n	   WE ARE EAGERLY LOOKING FORWARD TO SERVE YOU AGAIN\n";
        cout << "\n			    HAVE A NICE DAY !\n\n";
        cout <<"         ESTIMATED FOOD PREPARATION TIME :  "<<maxeta<< endl;
        maxeta=0;
        cin.get();
    }
    else
    {
        cout << "**************************  YOUR ID IS WRONG  ***************************" << endl;
        // cust_menu1();
    }
}

//function to display heading of customer details
void customer_detail_heading()
{
      
      cout<<"                        CUSTOMER DETAILS                                "<<endl;
      
      cout<<"CUST.NO"<<setw(13)<<"NAME"<<setw(23)<<"ADDRESS"<<setw(27)<<"PHONE NO"<<endl;
      cout<<"-------------------------------------------------------------------------"<<endl;
}
void administratormenu()
{
      char ch;
      do
      {
            cout<<endl;
            cout<<"                  ADMINISTRATOR MENU                   "<<endl;
            cout<<"1. CUSTOMERS "<<endl;
            cout<<"2. PRODUCTS "<<endl;
            cout<<"3. GO BACK"<<endl;
            cout<<"========================================================================"<<endl;
            cout<<"Please Select Your Option (1-3) "<<endl;
            cin>>ch;
                  switch(ch)
                  {
                        case '1':
                              customer_menu();
                              break;
                        case '2':
                              product_menu();
                              break;
                        case '3':
                              identify();
                              break;
                        default :
                              cout<<"Please enter valid option"<<endl;
                  }
      }while(ch!='3');
}
void userLogin()
{
    label:
    displayCommon();
    cout << "1.REGISTER (If you are new)" << endl;
    cout << "2.LOGIN (If you already registered)" << endl;
    cout << "3.EXIT" << endl;
    string p, q, w;
    int x, flag0 = 0;
    string s;
    char kk;
    cin >> x;
    if (x == 1)
    { write_customer();
        label0:
        flag0 = 0;
        ofstream fout;
        cout << "SET YOUR LOGIN USERNAME" << endl;
        cin >> s;
        cout << "SET YOUR PASSWORD" << endl;
        cin >> p;
        s = s + p;
        ifstream fin;
        string line;
        int offset = 0;
        fin.open("Userfile.txt");
        if (fin.is_open())
        {
            while (!fin.eof())
            {
                getline(fin, line);
                if (line.find(s, 0) != -1)
                {
                    cout << "Sorry! This username is not available" << endl;
                    cout << "Press try again!!" << endl;
                    flag0 = 1;
                    goto label0; // Remove label set it to menu
                    
                    // break;
                }
            }
        }
        if (flag0 == 0)
        {
            cout << "Registered Successfully" << endl;
            flag0 = 0;
        }
        fin.close();
        fout.open("Userfile.txt", ios::app);
        fout << s + "\n";
        fout.close();
        goto label1;
    }
    else if (x == 2)
    {
    label1:
        string line;
        ifstream fin;
        int offset = 0;
        cout << ".........................PLEASE ENTER YOU LOG IN CREDENTIALS !!............................." << endl;
        cout << "Enter your Login Username :" << endl;
        cin >> q;
        cout << "Enter your Password :" << endl;
        cin >> w;
        q = q + w;
        fin.open("UserFile.txt");
        if (fin.is_open())
        {
            while (!fin.eof())
            {
                getline(fin, line);
                if (line.find(q, 0) != -1)
                {
                    cout << "Logged in Successfully !!" << endl;
                    offset = 1;
                    place_order(q);
                    break;
                }
            }
            if (offset == 0)
            {
                cout << "Sorry, You are unauthorised!!" << endl;
                cout << "Press any key and enter" << endl;
                char o;
                cin >> o;
                goto label1;
            }
        }
        fin.close();
    }
    else if (x == 3)
    {
        exit;
    }
    else
    {   cout<<"Didn't get you!!!! Press valid Number"<<endl;
        goto label;
    }
}
string create_file(string name_user){
    string extension = ".txt";
    string temp=name_user+extension;
    
    ofstream fin;
    fin.open(temp,ios::app);
    fin.close();
    return temp;
}
void displayUser()
{
}
void displayAdmin()
{
}
void authenticateUser()
{
    cout << "\t\t\t\t\tHELLO USER\n \t\t\t\t\tWELCOME TO\n";
    userLogin();
}
void adminLogin()
{ displayCommon();
    string q,w;
    label1:
        string line;
        ifstream fin;
        int offset = 0;
        cout << "......................PLEASE ENTER YOUR ADMIN LOG IN CREDENTIALS !!..........................." << endl;
        cout << "Enter your Login Username :" << endl;
        cin >> q;
        cout << "Enter your Password :" << endl;
        cin >> w;
        q = q + w;
        fin.open("adminPass.txt");
        if (fin.is_open())
        {
            while (!fin.eof())
            {
                getline(fin, line);
                if (line.find(q, 0) != -1)
                {
                    cout << "Logged in Successfully !!" << endl;
                    offset = 1;
                    administratormenu();
                    break;
                }
            }
            if (offset == 0)
            {
                cout << "Sorry, You are unauthorised!!" << endl;
                cout << "Press any key and enter" << endl;
                char o;
                cin >> o;
                goto label1;
            }
        }
        fin.close();
    
}
void authenicateAdmin()
{
    cout << "\t\t\t\t\tHELLO ADMIN\n \t\t\t\t\tWELCOME TO\n";
    adminLogin();
  
}
void identify()
{
    int x;
    cout << "Press 1. If you are user\nPress 2. If you are admin\nPress any key..To exit\n";
    cin >> x;
    if (x == 1)
    {
        authenticateUser();
    }
    else if (x == 2)
    {
        authenicateAdmin();
    }
    else
    {
        exit;
    }
}
int main()
{
    displayCommon();
    identify();
}
    